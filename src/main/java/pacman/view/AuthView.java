package pacman.view;

import pacman.Logger;
import pacman.controller.NetworkController;
import pacman.mvc.Controller;
import pacman.mvc.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AuthView extends View {

    private JButton connectButton = new JButton("Connection");
    private JButton offlineButton = new JButton("Offline");

    private JPanel topPanel = new JPanel();
    private JTextField usernameTextField = new JTextField();
    private JTextField passwordTextField = new JTextField();

    private JPanel botPanel = new JPanel();

    public AuthView(){
        this.setSize(300, 100);
        this.setLayout(new GridLayout(2, 1));

        this.add(topPanel);
        this.add(botPanel);

        topPanel.setLayout(new GridLayout(1, 2));
        topPanel.add(usernameTextField);
        topPanel.add(passwordTextField);

        botPanel.setLayout(new GridLayout(1, 2));
        botPanel.add(connectButton);
        botPanel.add(offlineButton);

        connectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                controller.sendInput(Controller.Input.CNCT, usernameTextField.getText() + " " + passwordTextField.getText());
            }
        });

        offlineButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // start ADVANCED controller
                //Logger.getInstance().send("AuthView", "offline mode started");
            }
        });

        this.setVisible(true);
    }

    @Override
    public void update(Event event) {

    }
}
