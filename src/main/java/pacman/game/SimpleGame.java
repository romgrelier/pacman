package pacman.game;

import pacman.Logger;

public class SimpleGame extends Game {
    public SimpleGame(int maxTourCount) {
        super(maxTourCount);
    }

    @Override
    protected void initializeGame() {
        Logger.getInstance().send("SimpleGame", "Initialize Game");
    }

    @Override
    protected void takeTurn() {
        Logger.getInstance().send("SimpleGame", "takeTurn : " + tourCount + " / " + maxTourCount);
    }

    @Override
    protected void gameOver() {
        Logger.getInstance().send("SimpleGame", "Game Over");
    }
}
