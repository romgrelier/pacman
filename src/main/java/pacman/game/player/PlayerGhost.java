package pacman.game.player;

import pacman.command.Command;
import pacman.game.Maze;
import pacman.game.PacmanGame;
import pacman.game.actor.Actor;
import pacman.game.actor.Ghost;
import pacman.game.strategy.Dummy;

public class PlayerGhost extends Player {
    private Ghost ghost;

    public PlayerGhost(PacmanGame pacmanGame, Ghost ghost) {
        super(pacmanGame);
        this.ghost = ghost;
        strategy = new Dummy();
    }

    @Override
    public Command getNextCommand(Maze maze, PacmanGame pacmanGame) {
        moveCommand.setInvoker(ghost);
        moveCommand.setMaze(maze);
        moveCommand.setDirection(strategy.getNextDirection(ghost.getPosition(), pacmanGame));
        return moveCommand;
    }

    @Override
    public boolean canPlay() {
        return !ghost.isDead();
    }

    @Override
    public Actor getActor() {
        return ghost;
    }
}
