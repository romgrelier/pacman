package pacman.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import pacman.Logger;
import pacman.command.Command;
import pacman.command.CommandReceiver;
import pacman.command.game.*;
import pacman.game.Direction;
import pacman.game.Maze;
import pacman.game.PacmanGame;
import pacman.game.player.Player;
import pacman.observer.Observer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

public class NetworkController extends AdvancedController {
    private Command resetCommand = new ResetCommand();
    private Command runCommand = new RunCommand();
    private Command stepCommand = new StepCommand();
    private Command pauseCommand = new PauseCommand();
    private Command quitCommand = new QuitCommand();
    private SetSpeed setSpeedCommand = new SetSpeed();
    private SetMaze setMazeCommand = new SetMaze();

    private InputMoveCommand inputMoveCommand = new InputMoveCommand();
    private RequestPlayerCreate requestPlayerCreate = new RequestPlayerCreate();

    private Player player1 = null;
    private Player player2 = null;
    private Player player3 = null;

    private Socket socket;

    private PrintWriter output;
    private BufferedReader input;

    private String gameKey;

    private AtomicBoolean isRunning = new AtomicBoolean(true);

    public NetworkController() {
        try {
            this.socket = new Socket("localhost", 3000);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            output = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void play() {
        // Reception thread
        Thread readInput = new Thread(() -> {
            while (!socket.isClosed() && isRunning.get()) {

                try {
                    String in = input.readLine();

                    if (in.split(" ")[0].equals(":pacman")) {
                        if (in.split(" ")[1].equals("STATE")) {
                            GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();

                            Maze serverMaze = gson.fromJson(in.substring(":pacman STATE".length()), Maze.class);
                            PacmanGame localPacmanGame = ((PacmanGame) receiver);
                            Maze localMaze = localPacmanGame.getMaze();

                            Logger.getInstance().send("NetworkController", "Synchronization");

                            // synchro pacman
                            for (int i = 0; i < serverMaze.getPacman_start().size(); ++i) {
                                localMaze.getPacman_start().get(i).setX(serverMaze.getPacman_start().get(i).getX());
                                localMaze.getPacman_start().get(i).setY(serverMaze.getPacman_start().get(i).getY());
                                localMaze.getPacman_start().get(i)
                                        .setDirection(serverMaze.getPacman_start().get(i).getDirection());
                            }
                            // synchro ghost
                            for (int i = 0; i < serverMaze.getGhosts_start().size(); ++i) {
                                localMaze.getGhosts_start().get(i).setX(serverMaze.getGhosts_start().get(i).getX());
                                localMaze.getGhosts_start().get(i).setY(serverMaze.getGhosts_start().get(i).getY());
                            }
                            // sychro elements
                            localMaze.setMaze(serverMaze.getMaze());

                            ((PacmanGame) receiver).notifyObserver(Observer.Event.UPDATE);
                        } else if (in.split(" ")[1].equals("reset")) {
                            ((PacmanGame) receiver).notifyObserver(Observer.Event.RESTART);
                        } else if (in.split(" ")[1].equals("run")) {
                            ((PacmanGame) receiver).notifyObserver(Observer.Event.RUN);
                        } else if (in.split(" ")[1].equals("step")) {
                            ((PacmanGame) receiver).notifyObserver(Observer.Event.STEP);
                        } else if (in.split(" ")[1].equals("pause")) {
                            ((PacmanGame) receiver).notifyObserver(Observer.Event.PAUSE);
                        } else if (in.split(" ")[1].equals("end")) {
                            receiver.sendCommand(resetCommand);
                            Logger.getInstance().send("NetworkController", "end");
                        } else if (in.split(" ")[1].equals("map")) {

                        } else {
                            Logger.getInstance().send("NetworkController", "unknown command");
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        readInput.start();
    }

    public boolean connect(String username, String password) {
        output.println("auth connect " + username + " " + password);
        try {
            String response = input.readLine();

            if (response.split(" ")[0].equals("key")) {
                String key = response.split(" ")[1];
                if (key.equals("failed")) {
                    return false;
                } else {
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    private void join(String gameKey) {
        output.println("matchmaker join " + gameKey);
        play();
    }

    private void create() {
        output.println("matchmaker create");

        try {
            String response = input.readLine();
            if (response.split(" ")[0].equals("key")) {
                gameKey = response.split(" ")[1];
                Logger.getInstance().send("NetworkController", "game key received : " + gameKey);
                join(gameKey);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendMoveCommand(Player player, Direction direction) {
        inputMoveCommand.setDirection(direction);
        inputMoveCommand.setPlayer(player);

        switch (direction) {
        case NORTH:
            output.println(gameKey + " CHGDIR NORTH");
            break;
        case SOUTH:
            output.println(gameKey + " CHGDIR SOUTH");
            break;
        case EAST:
            output.println(gameKey + " CHGDIR EAST");
            break;
        case WEST:
            output.println(gameKey + " CHGDIR WEST");
            break;
        }

    }

    public void setPlayer(int id, Player player) {
        switch (id) {
        case 0:
            player1 = player;
            break;
        case 1:
            player2 = player;
            break;
        case 2:
            player3 = player;
            break;
        }
    }

    private void sendRequestPlayer(int id) {
        requestPlayerCreate.setController(this);
        requestPlayerCreate.setId(id);
        receiver.sendCommand(requestPlayerCreate);
    }

    @Override
    public void sendInput(Input input) {
        Logger.getInstance().send("NetworkController", input.toString());
        switch (input) {
        case Z:
            if (player1 != null) {
                sendMoveCommand(player1, Direction.NORTH);
            } else {
                sendRequestPlayer(0);
            }
            break;
        case Q:
            if (player1 != null) {
                sendMoveCommand(player1, Direction.WEST);
            } else {
                sendRequestPlayer(0);
            }
            break;
        case S:
            if (player1 != null) {
                sendMoveCommand(player1, Direction.SOUTH);
            } else {
                sendRequestPlayer(0);
            }
            break;
        case D:
            if (player1 != null) {
                sendMoveCommand(player1, Direction.EAST);
            } else {
                sendRequestPlayer(0);
            }
            break;
        case I:
            if (player2 != null) {
                sendMoveCommand(player2, Direction.NORTH);
            } else {
                sendRequestPlayer(1);
            }
            break;
        case J:
            if (player2 != null) {
                sendMoveCommand(player2, Direction.WEST);
            } else {
                sendRequestPlayer(1);
            }
            break;
        case K:
            if (player2 != null) {
                sendMoveCommand(player2, Direction.SOUTH);
            } else {
                sendRequestPlayer(1);
            }
            break;
        case L:
            if (player2 != null) {
                sendMoveCommand(player2, Direction.EAST);
            } else {
                sendRequestPlayer(1);
            }
            break;
        case RESET:
            player1 = null;
            player2 = null;
            player3 = null;
            // receiver.sendCommand(resetCommand);
            output.println(gameKey + " COM RESET");

            create();
            break;
        case RUN:
            // receiver.sendCommand(runCommand);
            output.println(gameKey + " COM RUN");
            break;
        case STEP:
            // receiver.sendCommand(stepCommand);
            output.println(gameKey + " COM STEP");
            break;
        case PAUSE:
            // receiver.sendCommand(pauseCommand);
            output.println(gameKey + " COM PAUSE");
            break;
        case QUIT:
            // receiver.sendCommand(quitCommand);
            output.println(gameKey + " COM QUIT");
            break;
        case SETSPEED:
            break;
        case CHGMAZE:
            break;
        default:
            Logger.getInstance().send("NetworkControlller", "Not handled input in Network Controller");
            break;
        }
    }

    public void sendInput(Input input, long number) {
        switch (input) {
        case SETSPEED:
            setSpeedCommand.setSpeed(number);
            // receiver.sendCommand(setSpeedCommand);
            break;
        default:
            Logger.getInstance().send("NetworkControlller", "Not handled input in Network Controller");
            break;
        }
    }

    public void sendInput(Input input, String string) {
        switch (input) {
        case CHGMAZE:
            setMazeCommand.setFile(string);
            receiver.sendCommand(setMazeCommand);
            output.println(gameKey + " MAP " + string);
            break;
        case CNCT:
            Logger.getInstance().send("NetworkControlller", "Connection...");
            String[] credentials = string.split(" ");
            if (connect(credentials[0], credentials[1])) {
                Logger.getInstance().send("NetworkControlller", "Connected");
                create();
                // close the connection window
            } else {
                Logger.getInstance().send("NetworkControlller", "Not connected");
            }
            break;
        default:
            Logger.getInstance().send("NetworkControlller", "Not handled input in Network Controller");
            break;
        }
    }

    @Override
    public void attachReceiver(CommandReceiver receiver) {
        this.receiver = receiver;
    }
}
