package pacman.controller;

import pacman.command.CommandReceiver;
import pacman.mvc.Controller;

public class FrontController extends Controller {
    public enum ControllerBehavior {
        BASIC, ADVANCED, NETWORKED, SERVER
    }

    private Controller controller;

    public FrontController(ControllerBehavior behavior) {
        setControllerBehavior(behavior);
    }

    public void setControllerBehavior(ControllerBehavior behavior) {
        switch (behavior) {
        case BASIC:
            controller = new BasicController();
            controller.attachReceiver(receiver);
            break;
        case ADVANCED:
            controller = new AdvancedController();
            controller.attachReceiver(receiver);
            break;
        case NETWORKED:
            controller = new NetworkController();
            controller.attachReceiver(receiver);
            break;
        case SERVER:
            controller = new ServerController();
            controller.attachReceiver(receiver);
            break;
        }
    }

    @Override
    public void sendInput(Input input) {
        // Logger.getInstance().send("FrontController", input.toString());
        controller.sendInput(input);
    }

    @Override
    public void sendInput(Input input, String string) {
        controller.sendInput(input, string);
    }

    @Override
    public void sendInput(Input input, long number) {
        controller.sendInput(input, number);
    }

    @Override
    public void attachReceiver(CommandReceiver receiver) {
        this.receiver = receiver;
        controller.attachReceiver(receiver);
    }
}
