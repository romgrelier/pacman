package pacman;

import pacman.command.game.ResetCommand;
import pacman.controller.FrontController;
import pacman.game.PacmanGame;
import pacman.mvc.Model;
import pacman.mvc.View;
import pacman.view.AuthView;
import pacman.view.CommandView;
import pacman.view.MazeGameView;

public class Application {
    public static void main(String[] args){
        Model model = new PacmanGame(1000);
        try {
            ((PacmanGame) model).changeMaze("src/main/resources/layout/contestClassic.lay");
        } catch (Exception e) {
            e.printStackTrace();
        }

        View gameView = new MazeGameView();
        gameView.attachModel(model);

        View commandView = new CommandView();
        commandView.attachModel(model);

        View authView = new AuthView();
        authView.attachModel(model);
        
        FrontController controller = new FrontController(FrontController.ControllerBehavior.NETWORKED);
        controller.attachReceiver(model);

        commandView.attachController(controller);
        gameView.attachController(controller);
        authView.attachController(controller);

        model.sendCommand(new ResetCommand());
    }
}
