package pacman.command.game;

import pacman.command.Command;
import pacman.game.Game;

public class StepCommand extends Command<Game> {

    @Override
    public void execute() {
        invoker.step();
    }

    @Override
    public void cancel() {

    }
}
