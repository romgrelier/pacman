package pacman.command;

public interface CommandInvoker {
    void attachReceiver(CommandReceiver receiver);
}
