package pacman.command;

public interface CommandReceiver {
    void sendCommand(Command command);
}
